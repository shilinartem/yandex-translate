//
//  API.swift
//  translate
//
//  Created by Artem Shilin on 08/05/2019.
//  Copyright © 2019 Artem Shilin. All rights reserved.
//

import Foundation

enum APIError : Error {
    case InvalidParams
    case InvalidJsonResponse
}

enum APIResponse {
    case success (_: Dictionary<String, Any>)
    case failture (_:Error?)
}

struct APIParams {
    let url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    let params: Dictionary<String, String>?
}

class API {
    func getRequest(params:APIParams, completion:@escaping(APIResponse) -> ()) {
        if let urlRequest = self.urlRequest(apiParams: params) {
            URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if let jsonData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: jsonData, options: [])
                        completion(.success(json as! Dictionary<String, Any>))
                    } catch {
                        completion(.failture(APIError.InvalidJsonResponse))
                    }
                    
                    
                } else {
                    completion(.failture(APIError.InvalidJsonResponse))
                }
            }.resume()
        } else {
            completion(.failture(APIError.InvalidParams))
        }
    }
    
    private func urlRequest(apiParams:APIParams) -> URLRequest? {
        if let params = apiParams.params {
            var stringParams = apiParams.url
            stringParams.append("?")
            
            for (key, value) in params {
                stringParams.append("\(key)=\(value)&")
            }
            
            if let url = URL(string: stringParams) {
                return URLRequest(url: url)
            }
        }
        
        return nil
    }
}

