//
//  TranslateViewModel.swift
//  translate
//
//  Created by Artem Shilin on 08/05/2019.
//  Copyright © 2019 Artem Shilin. All rights reserved.
//

import Foundation
// TODO remove private key
//
let yandexApiKey = "trnsl.1.1.20190506T151243Z.ba4ea10bc6469740.e4701a0f7112ab21579b187d6e139ada8a955aff"

enum TranslationDirection {
    case RUtoEN
    case ENtoRU

    var stringDirection: String {
        switch self {
        case .ENtoRU:
            return "en-ru"
        case .RUtoEN:
            return "ru-en"
        }
    }
}

protocol TranslateViewModelDelegate {
    func updateTranslateResult(text: String)
    func showLoading(show: Bool)
    func showErrorMessage(message: String)
    func changeLanguage(inLang: String, toLang: String)
}

class TranslateViewModel {
    var delegate: TranslateViewModelDelegate?
    private let api = API()
    private var direction = TranslationDirection.RUtoEN

    func configureViewModel() {
        self.changeTranslationDirection(direction: self.direction)
    }

    func translate(text: String) {
        if let apiParams = self.generateApiParams(text: text) {
            self.delegate?.showLoading(show: true)
            api.getRequest(params: apiParams) { (response) in
                DispatchQueue.main.async {
                    self.delegate?.showLoading(show: false)
                    switch response {
                    case .failture:
                        self.delegate?.showErrorMessage(message: "Произошла ошибка")
                    case let .success(data):
                        self.parseApiData(json: data)
                    }
                }
            }
        } else {
            self.delegate?.showErrorMessage(message: "Не заполнен текст")
        }
    }
    
    func swapTranslationDirection() {
        switch self.direction {
            case .ENtoRU:
                self.direction = .RUtoEN
            case .RUtoEN:
                self.direction = .ENtoRU
        }
        self.changeTranslationDirection(direction: self.direction)
    }

    private func changeTranslationDirection(direction: TranslationDirection) {
        self.direction = direction
        var inLanguageName = ""
        var toLanguageName = ""

        switch direction {
        case .ENtoRU:
            inLanguageName = "Английский"
            toLanguageName = "Русский"
        case .RUtoEN:
            inLanguageName = "Русский"
            toLanguageName = "Английский"
        }
        self.delegate?.changeLanguage(inLang: inLanguageName, toLang: toLanguageName)
    }

    // MARK: Private

    private func generateApiParams(text: String) -> APIParams? {
        if let rightText = text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            let params = [
                "key": yandexApiKey,
                "text": rightText,
                "lang": self.direction.stringDirection,
            ]
            let apiParams = APIParams(params: params)
            return apiParams;
        }
        return nil
    }

    // TODO error handling
    private func parseApiData(json: Dictionary<String, Any>) {
        let code = json["code"] as? Int
        if code == 200 {
            let translateTexts = json["text"] as? [String]
            if let translateText = translateTexts?.joined(separator: "\n") {
                self.delegate?.updateTranslateResult(text: translateText)
            }
        } else {
            self.delegate?.showErrorMessage(message: "Произошла ошибка")
        }
    }

}

