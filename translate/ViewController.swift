//
//  ViewController.swift
//  translate
//
//  Created by Artem Shilin on 08/05/2019.
//  Copyright © 2019 Artem Shilin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, TranslateViewModelDelegate {


    @IBOutlet weak var leftLanguageLabel: UILabel!
    @IBOutlet weak var rightLanguageLabel: UILabel!
    @IBOutlet weak var requestTranslateTextView: UITextView!
    @IBOutlet weak var responseTranslateTextView: UITextView!
    @IBOutlet weak var swapLanguagesButton: UIButton!
    @IBOutlet weak var sendRequestButton: UIButton!
    let viewModel = TranslateViewModel()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.responseTranslateTextView.isEditable = false
        self.viewModel.delegate = self
        self.viewModel.configureViewModel()
    }

    // MARK: - Button Action

    @IBAction func swapLanguagesButtonAction(_ sender: Any) {
        self.viewModel.swapTranslationDirection()
    }

    @IBAction func sendRequestButtonAction(_ sender: Any) {
        viewModel.translate(text: self.requestTranslateTextView.text)
    }

    // MARK: - TranslateViewModelDelegate

    func updateTranslateResult(text: String) {
        self.responseTranslateTextView.text = text
    }

    func showLoading(show: Bool) {
        // TODO
    }


    func showErrorMessage(message: String) {
     // TODO
    }

    func changeLanguage(inLang: String, toLang: String) {
        self.leftLanguageLabel.text = inLang
        self.rightLanguageLabel.text = toLang
    }

}

