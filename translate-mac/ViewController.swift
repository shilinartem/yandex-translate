//
//  ViewController.swift
//  translate-mac
//
//  Created by Artem Shilin on 13/05/2019.
//  Copyright © 2019 Artem Shilin. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, TranslateViewModelDelegate, NSTextFieldDelegate {
    @IBOutlet weak var progress: NSProgressIndicator!
    @IBOutlet weak var leftLanguageLabel: NSTextField!
    @IBOutlet weak var rightLanguageLabel: NSTextField!
    @IBOutlet weak var translatedTextView: NSTextField!
    @IBOutlet weak var swapLanguaguesButton: NSButton!
    @IBOutlet weak var translateButton: NSButton!
    @IBOutlet weak var resultTextLabel: NSTextField!
    let viewModel = TranslateViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.delegate = self
        self.translatedTextView.delegate = self
        self.viewModel.configureViewModel()
        self.translatedTextView.stringValue = ""
        self.resultTextLabel.stringValue = ""
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    // MARK: TranslateViewModelDelegate
    
    func updateTranslateResult(text: String) {
        self.resultTextLabel.stringValue = text
    }
    
    func showLoading(show: Bool) {
        if show {
            progress.startAnimation(nil)
        } else {
            progress.stopAnimation(nil)
        }
        self.translatedTextView.isEnabled = !show
        progress.isHidden = !show
        
    }
    
    func showErrorMessage(message: String) {
        let alert: NSAlert = NSAlert()
        alert.messageText = message
        alert.alertStyle = NSAlert.Style.warning
        alert.addButton(withTitle: "OK")
        alert.runModal()
    }
    
    func changeLanguage(inLang: String, toLang: String) {
        self.leftLanguageLabel.stringValue = inLang
        self.rightLanguageLabel.stringValue = toLang
    }

    
    @IBAction func swapLanguageButtonAction(_ sender: Any) {
        self.viewModel.swapTranslationDirection()
    }
    
    @IBAction func translateButtonAction(_ sender: Any) {
        self.translate()
    }
    
    private func translate () {
        if self.translatedTextView?.stringValue != nil && !(self.translatedTextView?.stringValue.isEmpty)! {
            self.viewModel.translate(text: self.translatedTextView.stringValue)
        }
    }
    
    func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
        
        if (commandSelector == #selector(NSResponder.insertNewline(_:))) {
            self.translate()
            return true
        }

        return false
    }
}

