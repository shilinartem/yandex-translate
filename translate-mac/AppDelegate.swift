//
//  AppDelegate.swift
//  translate-mac
//
//  Created by Artem Shilin on 13/05/2019.
//  Copyright © 2019 Artem Shilin. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

